import products from "../models/products";
import reviews from "../models/reviews";

function getProductDetails(request, response) {
  response.header("Content-Type", "application/json");
  const productId = Number(request.params.id);
  if (!productId) response.send("Product Id is missing");
  let searchedProduct = products.find(product => product.id === productId);
  if (!searchedProduct)
    searchedProduct = {
      status: 404,
      message: `No product found with product id ${productId}`
    };
  response.send(JSON.stringify(searchedProduct, null, 4));
}

function getProductReviews(request, response) {
  response.header("Content-Type", "application/json");
  const productId = Number(request.params.id);
  if (!productId) response.send("Product Id is missing");
  const searchedReviews = reviews.filter(
    review => review.productId === productId
  );
  response.send(JSON.stringify({ reviews: searchedReviews }, null, 4));
}

export default { getProductDetails, getProductReviews };
