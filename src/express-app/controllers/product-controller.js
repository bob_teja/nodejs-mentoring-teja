import products from "../models/products";

function getAllProducts(request, response) {
  response.header("Content-Type", "application/json");
  response.send(JSON.stringify({ products }, null, 4));
}

function addProduct(request, response) {
  response.header("Content-Type", "application/json");
  products.push(request.body);
  response.send(request.body);
}

export default { getAllProducts, addProduct };
