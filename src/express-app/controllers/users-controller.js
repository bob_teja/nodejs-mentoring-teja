import users from "../models/users";

function getAllUsers(request, response) {
  response.header("Content-Type", "application/json");
  response.send(JSON.stringify({ users }, null, 4));
}

export default { getAllUsers };
