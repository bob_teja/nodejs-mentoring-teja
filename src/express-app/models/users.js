const users = [
  {
    id: 1,
    name: "Jack"
  },
  {
    id: 2,
    name: "Jill"
  },
  {
    id: 3,
    name: "James"
  }
];

export default users;
