const products = [
  {
    id: 1,
    name: "T-Shirt",
    brand: "Supreme",
    price: 99.99,
    options: [{ color: "blue" }, { size: "XL" }]
  },
  {
    id: 2,
    name: "Jeans",
    brand: "Supreme",
    price: 599.99,
    options: [{ color: "black" }, { size: "L" }]
  },
  {
    id: 3,
    name: "Casual Wear",
    brand: "Supreme",
    price: 399.99,
    options: [{ color: "red" }, { size: "M" }]
  }
];

export default products;
