const config = {
  htmlServerMeta: {
    title: "HTML Server",
    head: "HTML Server",
    message: "Welcome from HTML Server"
  },
  htmlStreamServerMeta: {
    title: "HTML Stream Server",
    head: "HTML Stream Server",
    message:
      "Welcome from HTML Stream Server. You have passed the --stream argument"
  }
};

export default config;
